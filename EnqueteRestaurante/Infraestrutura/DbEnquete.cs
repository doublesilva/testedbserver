﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;

namespace Infraestrutura
{
    public class DbEnquete
    {
        private static DbEnquete instancia;
        private DbEnquete()
        {
            Inicializa();
        }

        private void Inicializa()
        {
            Profissionais = new List<Profissional>();
            Profissionais.Add(new Profissional()
            {
                IdProfissional = 1,
                Nome = "Pedro"
            });

            Profissionais.Add(new Profissional()
            {
                IdProfissional = 2,
                Nome = "Paulo"
            });


            Profissionais.Add(new Profissional()
            {
                IdProfissional = 3,
                Nome = "Paula"
            });

            Profissionais.Add(new Profissional()
            {
                IdProfissional = 4,
                Nome = "Patricia"
            });


            Restaurantes = new List<Restaurante>();

            Restaurantes.Add(new Restaurante()
            {
                IdRestaurante = 1,
                Nome = "Espaço 11"
            });

             Restaurantes.Add(new Restaurante()
            {
                IdRestaurante = 2,
                Nome = "Espaço 50"
            });

             Restaurantes.Add(new Restaurante()
            {
                IdRestaurante = 3,
                Nome = "Panorama"
            });


             Restaurantes.Add(new Restaurante()
            {
                IdRestaurante = 4,
                Nome = "Espaço 32"
            });


             Restaurantes.Add(new Restaurante()
            {
                IdRestaurante = 5,
                Nome = "Silva"
            });

            Enquetes = new List<Enquete>();

            Enquetes.Add(new Enquete()
            {
                DtVoto = new DateTime(2014, 07, 17),
                Escolhido = Restaurantes[1],
                Votante = Profissionais[0]
            });

        }

        public static DbEnquete Instancia
        {
            get
            {
                if (instancia == null)
                    instancia = new DbEnquete();
                return instancia;
            }
        }

        public IList<Enquete> Enquetes
        {
            get;
            set;
        }

        public IList<Restaurante> Restaurantes
        {
            get;
            set;
        }

        public IList<Profissional> Profissionais
        {
            get;
            set;
        }
    }
}
