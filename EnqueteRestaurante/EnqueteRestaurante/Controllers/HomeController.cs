﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repositorio;
using Entidades;
using System.IO;

namespace EnqueteRestaurante.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            RepositorioProfissional repProfissional = new RepositorioProfissional();
            ViewBag.Profissionais = new SelectList(repProfissional.GetProfissionais(), "IdProfissional", "Nome"); ;

            return View();
        }
        
              
        public ActionResult Resultado()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Regularizado(string idProfissional)
        {
            
            if (!string.IsNullOrEmpty(idProfissional))
            {
                RepositorioEnquete repEnquete = new RepositorioEnquete();
                if (repEnquete.PodeVotar(Convert.ToInt32(idProfissional)))
                {

                    return Json(JsonResponseFactory.SuccessResponse(RenderPartialViewToString("_Restaurantes")), JsonRequestBehavior.AllowGet);
                }
                else return Json(JsonResponseFactory.ErrorResponse("Já votou hoje!!"), JsonRequestBehavior.AllowGet);
            }
            else return Json(JsonResponseFactory.ErrorResponse("Selecione um profissional!!"), JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public ActionResult Votar(string idRestaurante, string idProfissional)
        {
            try
            {
                RepositorioEnquete enquete = new RepositorioEnquete();
                if (enquete.Votar(Convert.ToInt32(idRestaurante), Convert.ToInt32(idProfissional)))
                {
                    return Json(JsonResponseFactory.SuccessResponse(), JsonRequestBehavior.AllowGet);
                }
                else return Json(JsonResponseFactory.ErrorResponse("Não foi possivel computar seu voto."), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(JsonResponseFactory.ErrorResponse(ex.Message), JsonRequestBehavior.AllowGet);
            }
            
        }


        protected string RenderPartialViewToString(string viewName)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");


            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

    }
}
