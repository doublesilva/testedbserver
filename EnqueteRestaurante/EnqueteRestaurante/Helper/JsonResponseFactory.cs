﻿public class JsonResponseFactory
{
    public static object ErrorResponse(string error)
    {
        return new { Success = false, ErrorMessage = error };
    }

    public static object SuccessResponse(string html =null)
    {
        return new { Success = true, Html = html };
    }

    public static object SuccessResponse(object referenceObject)
    {
        return new { Success = true, Object = referenceObject };
    }




}