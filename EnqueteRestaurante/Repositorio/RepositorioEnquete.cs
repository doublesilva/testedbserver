﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infraestrutura;
using Entidades;
using Entidades.Util;
namespace Repositorio
{
    public class RepositorioEnquete
    {


        public bool Votar(int idRestaurante, int idProfissional)
        {
            if (!Elegivel(idRestaurante))
            {
                throw new Exception("Restaurante já foi escolhido esta semana");
            }
            if (!PodeVotar(idProfissional))
            {
                throw new Exception("Já votou hoje, agora só amanhã.");
            }
            DbEnquete.Instancia.Enquetes.Add(new Enquete()
            {
                DtVoto = DateTime.Now,
                Escolhido = DbEnquete.Instancia.Restaurantes.Where(x => x.IdRestaurante == idRestaurante).FirstOrDefault(),
                Votante = DbEnquete.Instancia.Profissionais.Where(x => x.IdProfissional == idProfissional).FirstOrDefault()
            });
            return true;


        }

        public bool Elegivel(int idRestaurante)
        {
            var escolhidos = EscolhidosByWeek(DateTime.Now.WeekOfYear());
            return escolhidos.Count > 0 ? !escolhidos.Any(x => x.IdRestaurante == idRestaurante) : true;
        }

        public bool PodeVotar(int idProfissional)
        {
            return DbEnquete.Instancia.Enquetes.Count > 0 ? !DbEnquete.Instancia.Enquetes.Any(x => x.Votante.IdProfissional == idProfissional && x.DtVoto.Date == DateTime.Now.Date) : true;
        }


        public Dictionary<Restaurante, int> ResultadoParcial()
        {
            Dictionary<Restaurante, int> resultado = new Dictionary<Restaurante, int>();
            if (DbEnquete.Instancia.Enquetes.Count > 0)
            {
                var votos = DbEnquete.Instancia.Enquetes.Where(x => x.DtVoto.WeekOfYear() == DateTime.Now.WeekOfYear() && x.DtVoto.Year == DateTime.Now.Year && x.DtVoto.Day == DateTime.Now.Day)
                    .Select(y => new { IdRestaurante = y.Escolhido.IdRestaurante })
                    .GroupBy(z => z.IdRestaurante).Select(v => new { IdRestaurante = v.Key, Votos = v.Count() }).OrderBy(x => x.Votos);



                foreach (var voto in votos)
                {
                    resultado.Add(DbEnquete.Instancia.Restaurantes.Where(x => x.IdRestaurante == voto.IdRestaurante).First(), voto.Votos);
                }

            }

            return resultado;
        }

        public List<Restaurante> EscolhidosByWeek(int numberWeek, int year = 0)
        {
            if (year == 0) year = DateTime.Now.Year;
            List<Restaurante> resultado = new List<Restaurante>();
            if (DbEnquete.Instancia.Enquetes.Count > 0)
            {
                var votos = DbEnquete.Instancia.Enquetes.Where(x => x.DtVoto.WeekOfYear() == numberWeek && x.DtVoto.Year == year && x.DtVoto.Day < DateTime.Now.Day)
                    .Select(y => new { IdRestaurante = y.Escolhido.IdRestaurante, Day = y.DtVoto.Day })
                    .GroupBy(z => new { z.Day, z.IdRestaurante })
                    .Select(v => new { IdRestaurante = v.Key.IdRestaurante, Day = v.Key.Day, Votos = v.Count() }).OrderBy(x => new { x.Day, x.Votos });



                foreach (var dia in votos.GroupBy(x => x.Day))
                {
                    var voto = votos.Where(x => x.Day == dia.Key).First();

                    resultado.Add(DbEnquete.Instancia.Restaurantes.Where(x => x.IdRestaurante == voto.IdRestaurante).First());
                }

            }

            return resultado;
        }
    }
}
