﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Infraestrutura;
namespace Repositorio
{
    public class RepositorioRestaurante
    {
        public List<Restaurante> GetRestaurantes()
        {
            return DbEnquete.Instancia.Restaurantes.ToList();
        }

        public Restaurante GetById(int idRestaurante)
        {
            return GetRestaurantes().Where(x => x.IdRestaurante == idRestaurante).FirstOrDefault();
        }
    }
}
