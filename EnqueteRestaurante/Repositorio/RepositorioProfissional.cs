﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Infraestrutura;
namespace Repositorio
{
    public class RepositorioProfissional
    {
        public List<Profissional> GetProfissionais()
        {
            return DbEnquete.Instancia.Profissionais.ToList();
        }

        public Profissional GetById(int idProfissional)
        {
            return GetProfissionais().Where(x => x.IdProfissional == idProfissional).FirstOrDefault();
        }
    }
}
