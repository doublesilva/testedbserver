﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Profissional
    {
        public int IdProfissional { get; set; }
        public string Nome { get; set; }
    }
}
