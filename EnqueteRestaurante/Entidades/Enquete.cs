﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Enquete
    {
        public int IdEnquete { get; set; }
        public DateTime DtVoto { get; set; }
        public Profissional Votante {get; set;}
        public Restaurante Escolhido { get; set; }
    }
}
