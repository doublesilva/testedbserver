# O que vale destacar no código implementado? #

### Criei uma arquitetura multi camada sendo elas: Entidade, Infraestrutura, Repositorio e Interface. ###
* Na camada de Entidade eu criei três entidades Profissional, Restaurante e Enquete. 
* Na camada de Infraestrutura criei uma classe para servir como minha basedados(seria meu DBContext), implementei ela no padrão singleton para ter uma só instância no meu projeto.
* Na camada de Repositorio implementei os critérios de aceitação do sistema.
* Na camada de Interface utilizei MVC com View Egine Razor.


#O que poderia ser feito para melhorar o sistema? #
### Poderia ter ajustado o layout da página. ###